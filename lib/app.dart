import 'package:flutter/material.dart';
import 'package:onesignal_flutter/onesignal_flutter.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'constant.dart';

class App extends StatefulWidget {
    @override
    AppState createState() => AppState();
}

class AppState extends State<App> {

    Future<SharedPreferences> futurePref = SharedPreferences.getInstance();
    Future<bool> isPushNotification;

    @override
    void initState() {
        super.initState();
        initPlatformState();
        isPushNotification = futurePref.then((SharedPreferences pref) {
            return pref.getBool(IS_SUBSCRIBE_NOTIFICATION) ?? true;
        });
    }

    @override
    Widget build(BuildContext context) {
        return MaterialApp(
            title: 'Flutter Demo',
            theme: ThemeData(primarySwatch: Colors.blue),
            home: Scaffold(
                appBar: AppBar(
                    title: const Text('Push Notification'),
                ),
                body: Container(
                    padding: const EdgeInsets.all(16.0),
                    child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                            const Text('Recieve Notification'),
                            FutureBuilder<bool>(
                                future: isPushNotification,
                                builder: (BuildContext context, AsyncSnapshot<bool> snapshot) {
                                    if (snapshot.hasData) {
                                        return Switch(
                                            value: snapshot.data,
                                            onChanged: (bool value) {
                                                OneSignal.shared.setSubscription(value);
                                                futurePref.then((SharedPreferences pref) {
                                                    pref.setBool(IS_SUBSCRIBE_NOTIFICATION, value);
                                                });
                                            },
                                        );
                                    } else {
                                        return const Center(
                                            child: CircularProgressIndicator()
                                        );
                                    }
                                },
                            ),

                        ]
                    )
                )
            )
        );
    }

    Future<void> initPlatformState() async {
        if (mounted) {
            OneSignal.shared.setLogLevel(OSLogLevel.verbose, OSLogLevel.none);
            await OneSignal.shared.init(
                'c3deb1e8-4c0c-42f9-b7e3-35ec554df2e3',
                iOSSettings: <OSiOSSettings, dynamic>{
                    OSiOSSettings.autoPrompt: false,
                    OSiOSSettings.inAppAlerts: true
                }
            );
            OneSignal.shared.setInFocusDisplayType(OSNotificationDisplayType.notification);
            // sample for send tag
            /*OneSignal.shared.sendTag('userid', 'u001').then((response) {
                print('App # Success send Tag $response');
            }).catchError((error) {
                print('App # Error send Tag $error');
            });*/
        }
    }

}
